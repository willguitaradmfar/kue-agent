const os = require("os");
const kue = require('kue');
const redis = require('redis');
const fs = require("fs");
const cluster = require("cluster");
const exec = require("child_process").exec;
const numCPUs = require('os').cpus().length;

const filesAgent = '/tmp/kue-agent';

const redisConfig = {
    port: process.env.REDISPORT || 6379,
    host: process.env.REDISHOST || '127.0.0.1'
}

const queue = kue.createQueue({
    prefix: 'will',
    redis: redisConfig
});

if (cluster.isMaster) {


    const subscribe = redis.createClient(redisConfig);
    const publisher = redis.createClient(redisConfig);
    subscribe.subscribe('req-file');

    const reqFile = () => {

        let job = queue.create('req-file', {
            host: os.hostname(),
            pid: process.pid
        }).removeOnComplete(true).priority('high').save(err => {
            if (err)
                console.log(`ERRO: ${err}`);
        }).on('complete', listFiles => {
            for (let file of listFiles) {
                fs.writeFile(`${filesAgent}/${file.name}`, file.data, (err) => {
                    if (err)
                        return console.log(`ERRO: ${err}`);
                    if (file.name == 'package.json') {
                        exec(`cd ${filesAgent} && npm install`, (error, stdout, stderr) => {
                            if (error)
                                return console.error(`exec error: ${error}`);

                            console.log(`stdout: ${stdout}`);
                            console.log(`stderr: ${stderr}`);
                        });
                    }

                    console.log(`Arquivo ${filesAgent}/${file.name} gravado`);
                    publisher.publish('req-file-done', JSON.stringify(file));

                });
            }
        }).on('failed', err => {
            console.log('ERRO:', err);
            setTimeout(reqFile, 1000 * 5);
        });
    }

    subscribe.on("message", function (channel, message) {
        reqFile();
    });

    reqFile();

    // Fork workers.
    for (var i = 0; i < numCPUs; i++) {
        cluster.fork();
    }

    cluster.on('exit', (worker, code, signal) => {
        console.log(`worker ${worker.process.pid} died`);
    });

} else {
    const subscribe = redis.createClient(redisConfig);
    subscribe.subscribe('req-file-done');

    subscribe.on("message", function (channel, file) {
        file = JSON.parse(file);
        if (fs.existsSync(`${filesAgent}/${file.name}`)) {
            console.log(`Deletando cache ${filesAgent}/${file.name}`);
            delete require.cache[require.resolve(`${filesAgent}/${file.name}`)];
        }
    });

    const PID = process.pid;

    queue.process('req-process', (job, done) => {

        job.progress(0, 100, `[${os.hostname()}] [${PID}] [${job.data.name}]`);

        try {
            let mod = require(`${filesAgent}/${job.data.name}`);


            if (typeof mod != 'function')
                return done(`[${os.hostname()}][AGENT][${PID}] O modulo ${job.data.name} não tem a function no export`);

            mod(job.data.data, (err, data) => {
                if (err)
                    return done(err);

                job.progress(100, 100, `[${os.hostname()}] [${PID}] [${job.data.name}]`);
                done(undefined, data);
            });
        } catch (err) {
            return done(err);
        }

    });
}


